/*
 * devID: device ID
 * dir: Direction
 * speed: Speed 
 * 
 * Device IDs:
 * MT: Magnetorquers
 * MT0,MT1,MT2,MT3
 * 
 * RW: Reaction Wheels
 * RWX,RWY,RWZ 
 * 
 * Ports: 
 * Use Texas Instrument MUX507 1:8 Dual channel(2:16)
 * 4 select lines - 
 * TrigA - 
 * TrigB - 
 * GND -> GND (Common Ground for Teensy,DEMUX and HBs)
 */


#include <SPI.h>

typedef struct {
  String type;
  uint8_t devID;
  bool dir;
  uint8_t sp;//speed(pwm)
} Command;


//H-Bridge 
class HbDriver{
  public:
  uint8_t trigA,trigB;

  HbDriver(uint8_t A, uint8_t B):trigA(A),trigB(B){
    
  }
  void init(){
    pinMode(trigA,OUTPUT);
    pinMode(trigB,OUTPUT);
  }
  /*
   * We always keep one side of H-bridge(B), locked to dir.
   * When dir is 1, 
   *  NMOS of B always conducts
   *  Load is HIGH when A is low 
   *    i.e. PMOS of A is conducting
   *  So we set A pwm(255-sp) for correct speed
   * When dir is 0, 
   *  PMOS of B always conducts
   *  Load is HIGH when A is HIGH
   *    i.e. NMOS of A is conducting
   *  So we set A pwm(sp) for correct speed
   * 
   */
  void go(bool d, uint8_t sp=255){
    digitalWrite(trigB,d);
    if(d){
      analogWrite(trigA,255-sp);
    }
    else analogWrite(trigA, sp);
  }
} ;
class Magnetorquer : public HbDriver{
  public:
  uint8_t devID;
  bool dir;
  uint8_t sp;

  Magnetorquer(uint8_t A,uint8_t B,uint8_t C): devID(A), HbDriver(B,C){
    
  }
  void init(){
    HbDriver::init();
    dir=0;
    sp=0;
    go(0,0);
  }
  void set(bool d, uint8_t s){
    dir=d;
    sp=s;
    go(dir,sp);
  }
}mt0(0,0,1),mt1(1,2,3),mt2(2,4,5);

class ReactionWheel : public Magnetorquer{
  public:
  ReactionWheel(uint8_t A,uint8_t B,uint8_t C):Magnetorquer(A,B,C){
    
  }
  //Add stuff here that is specific to rection wheels 
} rw0(5,6,7),rw1(6,8,9),rw2(7,10,11),rw3(8,99,98);

uint8_t decodeID(String devID){
  String inputString[]=
  { "MT0","MT1","MT2","MT3",
    "RW0","RW1","RW2","RW3","ALL" };
  uint8_t devIDs[]=
  { 0,1,2,3,
    5,6,7,8,10};

   for(uint8_t i=0;i<9;i++){
    if(inputString[i]==devID) return(devIDs[i]); 
   }
   return(255);
}
// reset function
void (*softwareReset) (void) = 0;

void setup() {
  AllZero();//initialize all rw and mt
  Serial.begin(115200);
  while (!Serial) {
    delay(1);
  }
  delay(100);
  Serial.println("READY");
}


Command comDecode(){
  //int temp;
  Command c;
  String current;
  c.type=Serial.readStringUntil(' ');
  if(Serial.available()>1){
    c.devID= decodeID(Serial.readStringUntil(' '));
    c.dir=(bool)Serial.readStringUntil(' ').toInt();
    if(Serial.available()>1) c.sp=(uint8_t)((Serial.readStringUntil(' ')).toInt());
    else {
      c.sp=0;
      Serial.read();
    }
  }
  else {
    Serial.read();
    c.type="SET";c.devID=10;c.dir=5;c.sp=0;
  }
  //ReadBack
  Serial.println("Decrypted Command");
  Serial.print(c.type);
  Serial.print(" ");
  Serial.print(c.devID);
  Serial.print(" ");
  Serial.print(c.dir); 
  Serial.print(" ");
  Serial.println(c.sp); 
  //Serial.print("Temp:");
  //Serial.println(temp);
  return(c);
}
void loop() {
  String commandIn;
  Command c;
  if(Serial.available()){
    //Read Command 
    c=comDecode();
    //Execute command
    if(c.type=="SET"){
       switch(c.devID){
        case 0: 
        Serial.println("MT0 actioned");
        mt0.set(c.dir,c.sp);
        break;
        case 1: 
        Serial.println("MT1 actioned");
        mt1.set(c.dir,c.sp);
        break;
        case 2: 
        Serial.println("MT2 actioned");
        mt2.set(c.dir,c.sp);
        break;
        case 5: 
        Serial.println("RW0 actioned");
        rw0.set(c.dir,c.sp);
        break;
        case 6: 
        Serial.println("RW1 actioned");
        rw1.set(c.dir,c.sp);
        break;
        case 7: 
        Serial.println("RW2 actioned");
        rw2.set(c.dir,c.sp);
        break;
        case 8: 
        Serial.println("RW3 actioned");
        rw3.set(c.dir,c.sp);
        break;
        case 10: 
        Serial.println("All 0");
        AllZero();
        break;
        default: 
        Serial.println("Default case");
        
      }
    }
  }
}
//Master RESET function
//Ensure any new object is added here as well.
void AllZero(){
  mt0.init();
  mt1.init();
  mt2.init();
  rw0.init();
  rw1.init();
  rw2.init();
  rw3.init();
}
